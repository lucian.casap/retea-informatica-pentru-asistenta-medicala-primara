# REȚEA INFORMATICĂ PENTRU ASISTENȚA MEDICALĂ PRIMARĂ - B'Connected

Aplicația B‘Connected va conlucra ca o singură platformă care gestionează programările, rețetele, rapoartele, istoricul medical al pacientului

## Ghid de Instalare

Acest ghid vă va ajuta să configurați proiectul pe Workstation-ul dumneavoastră local folosind XAMPP.

### Cerințe Preliminare

Înainte de a începe, asigurațivă că aveți următorul software instalat pe calculatorul dumneavoastră:

- [XAMPP](https://www.apachefriends.org/index.html): Un pachet gratuit și open-source de soluții pentru servere web, disponibil pentru mai multe platforme.

### Pași

Urmați acești pași pentru a instala și rula proiectul:

1. ## Descarcă și Instalează XAMPP## 

   - Dacă nu ați instalat încă XAMPP, descarcă-l de [aici](https://www.apachefriends.org/download.html) și urmați instrucțiunile de instalare pentru sistemul dumneavoastră de operare.
   - După instalare, lansați Panoul de Control XAMPP.

2. ## Descarcă Proiectul ## 

   - Navigați în directorul `htdocs` din cadrul instalării XAMPP.
    Acesta este localizat în:
     - **Windows:** `C:\xampp\htdocs`
     - **macOS:** `/Applications/XAMPP/htdocs`
     - **Linux:** `/opt/lampp/htdocs`
    - Creați un folder numit `bconnected` în `htdocs`

3. ## Pornește Serverul Apache ## 

   - Deschideți Panoul de Control XAMPP.
   - Apăsați butonul `Start` de lângă `Apache` pentru a porni serverul web Apache.

4. ## Configurarea Bazei de Date ## 

   - Porniți serviciul `MySQL` din Panoul de Control XAMPP.
   - Deschideți `phpMyAdmin` mergând la `http://localhost/phpmyadmin`.
   - Creați o nouă bază de date cu numele `asistenta_medicala_primara`.
   - Importați fișierul `asistenta_medicala_primara.sql` din folderul proiectului B'Connected în baza de date creată:
     1. În `phpMyAdmin`, selectează baza de date `asistenta_medicala_primara`.
     2. Mergeți la tab-ul `Import`.
     3. Apăsați pe `Choose File` și selectați fișierul `asistenta_medicala_primara.sql`.
     4. Apăsați butonul `Go` pentru a începe importul.

5. ## Accesează Site-ul Web ##

   - Deschideți browser-ul și mergeți la `http://localhost/bconnected`.
   - Ar trebui să puteți vedea acum site-ul funcționând corect.


## Repository GitLab

Acest proiect este găzduit pe GitLab și poate fi accesat la următoarea adresă:

[Repository GitLab](https://gitlab.upt.ro/lucian.casap/retea-informatica-pentru-asistenta-medicala-primara)
